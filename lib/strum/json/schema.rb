# frozen_string_literal: true

require "strum/json/schema/validate"
require "strum/json/schema/cast"

module Strum
  module Json
    module Schema
      class NoValue; end
    end
  end
end
